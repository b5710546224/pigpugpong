from django.conf.urls import url

from . import views

app_name = 'main'

urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^list',views.list,name='list'),
    url(r'^profile',views.profile,name='profile'),
    url(r'^history',views.history,name='history'),
    url(r'^login',views.login_regis_view,name='login_regis'),
    url(r'^doregis',views.do_regis,name='regis'),
    url(r'^dologin',views.do_login,name='login'),
    url(r'^profile',views.profile,name='profile'),
    url(r'^history',views.history,name='history'),
]
