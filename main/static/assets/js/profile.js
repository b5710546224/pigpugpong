var $input = $('.browse');
var $image = $('.profile');
var edit = $('#edit');
var save_change = $('#save_change');

save_change.hide();

$input.on('change', function() {
  var file = this.files[0];
  var reader = new FileReader();

  reader.onload = function(e) {
    $image.attr('src', e.target.result);
  };
  reader.readAsDataURL(file);
});

$('#edit-profile').on('submit',function(e) {
  e.preventDefault();
  save_change.show();
  edit.hide();
  console.log("yo");
});

$('#save-profile').on('submit',function(e) {
  e.preventDefault();
  edit.show();
  save_change.hide();
  console.log("yo");
});
