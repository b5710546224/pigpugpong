from django.shortcuts import render
from django.db import connection
from main.models import Accounts

from .models import Accounts

# Create your views here.
def index( request ) :
    return render( request,'index.html' )

def list( request ) :
    key = request.POST[ 'key' ]
    return render( request,'list.html', { 'key':key } )

def profile(request) :
    account = Accounts.objects.get(username='minsteric.smile')

def history(request) :
    account = Accounts.objects.get(username='minsteric.smile')
    return render(request,'history.html', {'account':account})

def login_regis_view( request ) :
    return render(request, 'login_register_view.html')

def do_login( request ) :
    if request.method == "POST" :
        _login_key = request.POST.get( "login_key" )
        _login_pass = request.POST.get( "login_password" )

        row = Accounts.objects.filter( username=_login_key, password=_login_pass )
        user = None
        if not row:
            row = Accounts.objects.filter( email=_login_key, password=_login_pass )

        if not row :
            request.session['error'] = "user not found"
            return render( request, 'login_register_view.html' )
        user = row[0]

        request.session['username'] = user.username

    return render( request, 'index.html' )

def do_regis( request ):

    _username = request.POST.get( "regis_username", "" )
    _email = request.POST.get( "regis_email", "" )
    _pass = request.POST.get( " regis_password", "" )
    _con_pass = request.POST.get( "regis_confirmpassword", "" )

    # if _pass == _con_pass:
    # if _pass != _con_pass:

    # new_account = Accounts()
    return HttpResponseRedirect('/login/')

def test_view( request ) :
    return render( request, "test.html" )
