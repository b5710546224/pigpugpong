from django.contrib import admin

from .models import Accounts,Order,Package,Top_Up_Type

admin.site.register(Accounts)
admin.site.register(Order)
admin.site.register(Package)
admin.site.register(Top_Up_Type)
