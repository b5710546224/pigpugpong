import datetime
from django.db import models

class Accounts (models.Model) :
    user_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=15)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=20)

class Order (models.Model) :
    order_id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey('Accounts')
    package_id = models.ForeignKey('Package')
    date_order = models.DateTimeField()
    date_paid = models.DateTimeField()
    date_count = models.IntegerField(3);
    status = models.BooleanField(default=False)
    top_up_code = models.CharField(max_length=13)
    payment_code = models.CharField(max_length=13)
#
# class User_Wallet (models.Model) :
#     user_id =
#     money =
#
class Package (models.Model) :
    package_id = models.AutoField(primary_key=True)
    top_up_type_id = models.ForeignKey('Top_Up_Type')
    value = models.IntegerField();
    price = models.IntegerField();

class Top_Up_Type (models.Model) :
    top_up_type_id = models.AutoField(primary_key=True)
    TYPE_NAME_CHOICES = (
        ('STREAM','stream'),
        ('LINE','line'),
        ('PLAY_STATION','play_station'),
        ('TOP_UP_WALLET','top_up_wallet')
    )
    type_name = models.CharField(max_length=20,choices=TYPE_NAME_CHOICES)
    image_path = models.CharField(max_length=100)
