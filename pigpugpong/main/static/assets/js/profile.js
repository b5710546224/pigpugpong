var $input = $('.browse');
var $image = $('.profile');

$input.on('change', function() {
  var file = this.files[0];
  var reader = new FileReader();

  reader.onload = function(e) {
    $image.attr('src', e.target.result);
  };
  reader.readAsDataURL(file);
});
