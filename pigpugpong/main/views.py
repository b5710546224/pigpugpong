from django.shortcuts import render

from .models import Accounts

# Create your views here.
def index(request) :
    return render(request,'index.html')

def list(request) :
    key = request.POST['key']
    return render(request,'list.html',{'key':key})

def profile(request) :

    account = Accounts.objects.get(username='minsteric.smile')

    return render(request,'profile.html', {'account':account})

# def profile(request) :
#     return render(request,'profile.html', {'key':key})
